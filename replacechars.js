// create a function that takes a string and replaces all it's letters with
// the letter after it in the alphabet.
function replace(str) {
  let newStr = "";

  for (var i = 0; i < str.length; i++) {
    newStr += (String.fromCharCode(str.charCodeAt(i) + 1));
  }

  return newStr;
}
console.log(replace("Hell"));
