// create a grading system for students
// the function should take a name, score and a type of exam.
// depending on the type of the exam the student gets different type of grading.
// if it's a finals exam his points have to be >=90 to have an A+
// if it's a normal exam and his points are between 89 and 100 he gets an A
// otherwise - print his name, total points and the exam type.
function gradeStudent(name, totalPoints, examType) {
  let finalResult = ``;
  let passCase1 = (examType == "Final-exam") && (
    totalPoints >= 90);
  let passCase2 = (examType == "Normal-exam") && (
    totalPoints >= 89 && totalPoints <= 100);


  if (passCase1) {
    finalResult = `${name} got A+ on the ${examType} : ${totalPoints} points.`;
  } else if (passCase2) {
    finalResult = `${name} got an A the ${examType} : ${totalPoints} points.`;
  } else {
    finalResult = `${name} got ${totalPoints} points on the ${examType}.`;
  }
  return finalResult
}

console.log(gradeStudent("Kristiyan", 100, "Final-exam"));
console.log(gradeStudent("Ivan", 89, "Normal-exam"));
console.log(gradeStudent("Mincho", 59, "Normal-exam"));
