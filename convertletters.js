// create a function that takes a string and orders it in alphabetical order
function convertLetters(str) {
  str = str.split("");

  for (var i = 0; i < str.length; i++) {
    if (str[i] > str[i+1]) {
      let temp = str[i + 1];
      str[i + 1] = str[i];
      str[i] = temp;
    }
  }
  return str.join("");
}

console.log(convertLetters("bac"));
