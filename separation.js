// craete a function that returns true or false depending on wheter of not there
// are atleast three chars between 'a' and 'b'
function separatedBy3(str) {
  return (/a...b/).test(str) || (/b...a/).test(str);
}
console.log(separatedBy3("Chainsbreaker"));
console.log(separatedBy3("pane borrowed"));
console.log(separatedBy3("abCheck"));
