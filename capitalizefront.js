// create a function that takes a sentence and capitalizes each letter of a
// separate word
function capitalize(sentence) {
  let newSentence = "";
  for (let i = 0; i < sentence.length; i++) {
    if (i > 0 && sentence.charCodeAt(i - 1) == 32 || i == 0) {
      newSentence += sentence[i].toUpperCase();
    } else {
      newSentence += sentence[i];
    }
  }
  return newSentence;
}


function capital_letter(sentence) {
  sentence = sentence.split("");

  for (var i = 0; i < sentence.length; i++) {
    sentence[i] = sentence[i][0].toUpperCase() + sentence[i].substr(1);
  }

  return sentence.join(" ");
}
console.log(capitalize("hello there"));
console.log(capital_letter("hello there"));
