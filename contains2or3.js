// create a function that takes an array and it returns true if the array
// contains either 1 or 3
function contains(arr) {
  if (arr.length == 2 && (arr.includes(1) || arr.includes(3))) {
    return true;
  }
  return false;
}
console.log(contains([1,2,3]));
console.log(contains([1]));
console.log(contains([1,2]));
console.log(contains([3,2]));
