// create a function that takes a string and returns a new one from the first half
// of the array, if the arrays length is an even number
function getHalf(str) {
  if (str.length % 2 != 0) {
    return;
  }
  return str.substring(0,str.length/2);
}

console.log(getHalf("abcd"));
