// Write a JavaScript program to find the kth greatest element of a given array of integers.
function findGreatest(arr, pos) {
  let max = -Infinity;
  for (var i = 0; i < pos; i++) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
}
console.log(findGreatest([1, 2, 3, 4, 5], 3))
console.log(findGreatest([-10, -25, -47, -36, 0], 1))
