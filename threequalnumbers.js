// create a program to find if 3 numbers are equal or not. If three of them
// equal return 30 if two of them are equal return 20. Otherwise - 40.
function areEqual(number1, number2, number3) {
  let counter = 1;
  let numbers = [number1, number2, number3];

  for (let i = 0; i < numbers.length; i++) {
    if(numbers[i] == numbers[i+1]){
      counter++;
    }
  }
  if(counter == 3){
    return 30;
  } else if(counter == 2){
    return 20;
  }
  return 40;
}

console.log(areEqual(2,2,2));
console.log(areEqual(2,2,3));
console.log(areEqual(1,2,3));
