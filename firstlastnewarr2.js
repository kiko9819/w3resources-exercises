// create a function that takes an array and returns a new one with both ends of
// the array.
function makeArr(arr){
  if(arr.length<1){
    return arr;
  }
  return [arr[0],arr[arr.length-1]];
}
console.log(makeArr([1,2,3]));
