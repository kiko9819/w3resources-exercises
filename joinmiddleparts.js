// given two arrays with a length of three, return a new array with the middle
// elements of both arrays.
function joinArraysMiddle(arr1, arr2) {
  if (arr1.length < 3 || arr2.length < 3) {
    return;
  }

  let newArr = [];
  let arr1Middle = arr1[Math.floor(arr1.length / 2)];
  let arr2Middle = arr2[Math.floor(arr2.length / 2)];

  newArr.push(arr1Middle, arr2Middle);

  return newArr;
}
const middleElements = (arrayA, arrayB) => {
  let newArr = [];

  newArr.push(arrayA[1], arrayB[1]);

  return newArr;
};
console.log(joinArraysMiddle([7, 8, 2], [1, 2, 3]));
console.log(middleElements([7, 8, 2], [1, 2, 3]));
