// create a function that takes two strings and returns a new one concatenated
// depending on what the difference between both strings length is.
// if one is larger than the other, concatenate the part of the larger string
// that it look like this: "hello" "abc" - > hellabc
function concatEqual(str1, str2) {
  let newStr1;
  let newStr2;
  let diff = Math.abs(str1.length - str2.length);

  if (diff > 0 && str1.length > str2.length) {
    newStr1 = "";

    for (let i = 0; i < str1.length - diff; i++) {
      newStr1 += str1[i];
    }

    newStr2 = str2;

    return newStr1.concat(newStr2);
  } else if (diff > 0 && str1.length < str2.length) {
    newStr2 = "";

    for (var i = 0; i < str2.length - diff; i++) {
      newStr2 += str2[i];
    }

    newStr1 = str1;

    return newStr2.concat(newStr1);
  }
  return str1.concat(str2);

}

console.log(concatEqual("abcd", "ab"));
console.log(concatEqual("ab", "ab"));
console.log(concatEqual("ab", "abcd"));
