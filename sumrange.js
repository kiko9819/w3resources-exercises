// create a function that takes two numbers and if the sum of those two numbers
// is in the range [50,80] return 65. Otherwise - 80.
function sumInRange(number1,number2){
  let sum = number1+number2;
  let range50_80 = sum>=50 && sum<=80;

  if(range50_80){
    return 65;
  }
  return 80;
}

console.log(sumInRange(20,35));
