// create a function that takes a number and returns a special difference if
// the input number is creater than 13
// to make it special you have to multiply the difference between 13 and the
// input number
// otherwise - return just the normal difference
function getDifference(number) {
  const NUMBER_13 = 13;
  if (number > NUMBER_13){
    return 2*Math.abs(number-NUMBER_13);
  }
  return Math.abs(number-NUMBER_13);
}

console.log(getDifference(100));
