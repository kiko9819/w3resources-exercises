// create a function that takes a number and returns a special difference if
// the input number is creater than 19
// to make it special you have to multiply the difference between 19 and the
// input number
// otherwise - return just the normal difference
function getSpecialDifference(inputNumber){
  const NUMBER_19 = 19;
  const MULTIPLICATOR = 3;

  if(inputNumber > NUMBER_19){
    return MULTIPLICATOR*(Math.abs(inputNumber-NUMBER_19));
  }
  return Math.abs(inputNumber-NUMBER_19);
}

console.log("The special difference is "+getSpecialDifference(25));
