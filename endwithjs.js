// create a function that tests if string ends in the word script
function endsInScript(str) {
  return /\w?Script$/g.test(str);
}
const endWith = (str, match) => {
  return (str.length >= 6) ? str.endsWith(match) : false;
};
console.log(endsInScript("ScriptaksjdfScript"));
console.log(endWith("JavaScript","Script"));
