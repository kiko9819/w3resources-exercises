// create a function that returns true if a string starts with java, otherwise - false
function startsWith(str) {
  let javaStr;
  if (str.length < 4) {
    return false;
  }
  javaStr = str.split("").slice(0, 4).join("");
  if (javaStr.toLowerCase() == "java") {
    return true;
  }
  return false;
}

console.log(startsWith("Javasctr"));
