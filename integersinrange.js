// create a function that take stwo integers and if either is in range [50,99]
// return true, otherwise - false
function inRange(number1, number2) {
  if (number1 == undefined || number1 == null ||
    number2 == undefined || number2 == null) {
    return false;
  }
  let number1Range = number1 <=99 && number1 >=50;
  let number2Range = number2 <=99 && number2 >=50;

  if(number1Range || number2Range){
    return true;
  }
  return false;
}

console.log("Both in range "+inRange(55,89));
console.log("First in range "+inRange(55,2));
console.log("Second in range "+inRange(2,90));
console.log("None in range "+inRange(2,9));
