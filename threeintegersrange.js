// create a program that given three integer numbers returns true if either of
// them is between 50 and 99.
function someInRange(number1, number2, number3) {
  if (number1 == undefined || number1 == null ||
    number2 == undefined || number2 == null ||
    number3 == undefined || number3 == null) {
    return false;
  }

  let number1Range = number1 <=99 && number1 >=50;
  let number2Range = number2 <=99 && number2 >=50;
  let number3Range = number3 <=99 && number3 >=50;

  if(number1Range || number2Range || number3Range){
    return true;
  }
  return false;
}

console.log(someInRange(1,2,3));
console.log(someInRange(55,2,3));
console.log(someInRange(5,55,3));
console.log(someInRange(5,5,55));
