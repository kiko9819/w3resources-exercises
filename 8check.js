// create a function that takes two numbers and checks if their sum, difference
// or either of them is a 8.
function intIsEight(number1,number2){
  let sum = (number1+number2) == 8;
  let diff = Math.abs(number1-number2) == 8;
  let isEight = (number1 == 8 || number2 == 8) || sum || diff;

  return isEight;
}

console.log(intIsEight(8,9));
console.log(intIsEight(8,8));
console.log(intIsEight(12,4));
