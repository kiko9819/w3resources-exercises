// sum the digits of a two digit number
const sumDigits = num => {
  return num.length == 2 ? Math.floor(num / 10 + num % 10) : "nope";
};

console.log(sumDigits(19));
