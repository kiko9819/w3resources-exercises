// create a function that takes two numbers and an operation and depending on
// that basic math operation execute the expression.
function operate(operand1, operand2, operation) {
  let operand1Missing = operand1 == undefined || operand1 == null;
  let operand2Missing = operand2 == undefined || operand2 == null;
  let operationMissing = operation == undefined || operation == null;

  if (operand1Missing || operand2Missing || operationMissing) {
    console.log("Something is missing.");
    return;
  }

  if (operation == "divide") {
    return operand1 / operand2;
  } else if(operation == "multiply") {
    return operand1 * operand2;
  } else {
    return "Incorrect operation.";
  }
}
console.log(operate(3, 4, "divide"));
console.log(operate(3, 4, "multiply"));
console.log(operate(3, 4, ""));
