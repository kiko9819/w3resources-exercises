// create a function that takes a string and return a new string with the last
// three chars of the original string attached to the front and end
function addLastThree(str){
  if(str.length<3){
    return "string too small";
  }
  if(!Number.isNaN(str)){
    str = str.toString();
  }

  let lastThreeChars = str.split("").slice(str.length-3).join("");

  return lastThreeChars+str+lastThreeChars;
}

console.log("With a number "+addLastThree(123));
console.log("With string "+addLastThree("123"));
console.log("With string "+addLastThree("Hello"));
