// create a function that takes three numbers and return the mode they are into
// look at the code and try figuring it out.
function whichMode(number1, number2, number3) {
  if (number2 > number1 && number3 > number2) {
    return "Strict mode";
  } else if ((number1 > number2 && number3 > number1) ||
    ((number1 == number2) && (number3 > number2))) {
    return "soft mode"
  } else {
    return;
  }
}

console.log(whichMode(10, 15, 31));
console.log(whichMode(24, 22, 31));
console.log(whichMode(22, 22, 31));
