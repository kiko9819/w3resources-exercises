// create a function that takes a string, checks if it begins with 'py'
// if it does it should return the rest of the string without 'py'
// otherwise attach 'py' to the front of the string and return it.
function attachPy(str){
  let frontOfStr = str.split("").slice(0,2).join("");
  let restOfStr = str.split("").slice(2).join("");

  if(frontOfStr === "py"){
      return restOfStr;
  }
  return "py"+str;
}
console.log(attachPy("pyhell"));
