// create a function that takes a number and returns true if it is within the
// 20s of 100 or 400.
function range(number) {
  const RANGE_20 = 20;

  return ((Math.abs(100 - number) <= RANGE_20) || (Math.abs(400-number)<=20));
}
console.log("100:");
console.log(range(110));
console.log(range(90));
console.log(range(79));
console.log();
console.log("400:");
console.log(range(420));
console.log(range(380));
console.log(range(820));
