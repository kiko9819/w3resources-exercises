// create a function that takes a string and if it starts or end with a P
// return a new string without the first and last letters
const startsEndWithP = str => {
  if(/^(p|P)|(p|P)$/g.test(str)){
    return str.substring(1,str.length-1);
  }
  return str;
};
console.log(startsEndWithP("help"));
console.log(startsEndWithP("phelp"));
console.log(startsEndWithP("phel"));
console.log(startsEndWithP("Phel"));
console.log(startsEndWithP("PhelP"));
console.log(startsEndWithP("PHP"));
