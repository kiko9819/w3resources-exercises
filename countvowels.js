// create a function that takes a string and returns the number of vowels in it
function countVowels(str) {
  let counter = 0;
  for (var i = 0; i < str.length; i++) {
    switch (str[i].toLowerCase()) {
      case "a":
      case "e":
      case "i":
      case "o":
      case "u":
        counter++;
    }
  }
  return counter;
}

console.log(countVowels("eqweriou"));
