// create a function that takes two numbers and checks if their sum, difference
// or either of them is a 15.
function intIsEight(number1,number2){
  let sum = (number1+number2) == 15;
  let diff = Math.abs(number1-number2) == 15;
  let isEight = (number1 == 15 || number2 == 15) || sum || diff;

  return isEight;
}

console.log(intIsEight(15,9));
console.log(intIsEight(25,15));
console.log(intIsEight(7,8));
console.log(intIsEight(25,10));
console.log(intIsEight(5,9));
console.log(intIsEight(7,9));
console.log(intIsEight(9,25));
