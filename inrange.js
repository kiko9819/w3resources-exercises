// create a function that takes two numbers and returns true if both are in range
// [40,60] or both are in range [70,100]
function inRange(number1, number2) {
  let range40_60 = (number1 >= 40 && number1 <= 60) &&
    (number2 >= 40 && number2 <= 60);
  let range70_100 = (number1 >= 70 && number1 <= 100) &&
    (number2 >= 70 && number2 <= 100);
  if (range40_60 || range70_100) {
    return true;
  }
  return false;
}

console.log(inRange(55, 2));
console.log(inRange(2, 55));
console.log(inRange(55, 55));
console.log(inRange(55, 75));
console.log(inRange(75, 55));
console.log(inRange(85, 2));
console.log(inRange(2, 85));
