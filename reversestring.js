// create a function that reverses a given string.
function reverse(str){
  return str.split("").reverse().join("");
}

console.log(reverse("123"));
