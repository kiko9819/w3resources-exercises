// create a function that takes a string and counts how many Ps and Ts it contains
// it they are equal return true, otherwise - false
function equalQuantity(str) {
  let pCount = 0;
  let tCount = 0;

  for (var i = 0; i < str.length; i++) {
    if (str[i] == "p") {
      pCount++;
    }
    if (str[i] == "t") {
      tCount++;
    }
  }
  return pCount == tCount ? true : false;
}

console.log(equalQuantity("paatpss"));
console.log(equalQuantity("paatps"));
console.log(equalQuantity("ptpt"));
