// craete a function that takes an array of integers and returns the sum
// of all those integers

// decided to make two solutions, just for variety's sake.
const sumReduce = arr => {
  if(arr.length<3){
    return "Array too short";
  }
  return arr.reduce((prev, next) => {
    return prev + next;
  });
}

function sumFor(arr) {
  let sum = 0;
  for (var i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  return sum;
}
console.log(sumReduce([1, 2, 3]));
console.log(sumFor([1, 2, 3]));
