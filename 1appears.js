// create a function that takes two arguments an array and a number 1.
// the function should tell if the first or last element in the array is equal
// to the 1
function appearsIn(arr, one) {
  if (arr[0] == one || arr[arr.length - 1] == one) {
    return true;
  }
  return false;
}

console.log(appearsIn([1, 2, 3], 1));
console.log(appearsIn([1, 2, 1], 1));
console.log(appearsIn([0, 2, 1], 1));
