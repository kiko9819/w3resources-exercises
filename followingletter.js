// create a function that takes a word and returns a new one with all the
// letters shifted right in the alphabet.
function shiftLetters(word) {
  if(word.length == 0){
    return "Input a string";
  }
  let newWord = "";

  for (let i = 0; i < word.length; i++) {
    // makes sure it's not a uppercase or a lowercase 'Z'
    if (word.charCodeAt(i) != 122 && word.charCodeAt(i) != 90) {
      newWord += String.fromCharCode(word.charCodeAt(i) + 1);
    } else {
      newWord += "a";
    }
  }
  return newWord;
}

console.log(shiftLetters("PYTHON"));
console.log(shiftLetters("W3R"));
console.log(shiftLetters("php"));
console.log(shiftLetters(""));
