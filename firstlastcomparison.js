// create a function that takes an array and compares the first and last elements
// fill a new array with the largest element from those compared
// the new array should be the same size as the original
function findLargest(arr) {
  let newArr = [];
  if (arr.length < 2) {
    return;
  }
  if (arr[0] > arr[arr.length - 1]) {
    for (let i = 0; i < arr.length; i++) {
      newArr.push(arr[0]);
    }
    return newArr;
  }
  for (let i = 0; i < arr.length; i++) {
    newArr.push(arr[arr.length-1]);
  }
  return newArr;
}

console.log(findLargest([1,2,3,4,5]));
