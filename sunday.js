// write a function that takes a year and goes all the way from that year to
// the year 2050 and says on which year the first day of january is a sunday.
function isSunday(year) {
  let result;
  for (let i = year; i < 2050; i++) {
    let currentYear = new Date(i, 0, 1);
    result = currentYear.getDay() === 0 ? i : false;
    if(result){
      console.log(`${i} `);
    }
  }
}
isSunday(2011);
