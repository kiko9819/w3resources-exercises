// create a function that takes a month, date and year and tells how much days
// until next christmas
function isLeapYear(year) {
  if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
    return true;
  }
  return false;
}

function nextChristmas(currentMonth, currentDate, year) {
  let currentDays = 0;
  let currenMonthDaysElapsed = new Date(`${currentMonth} ${currentDate}`).getDate();
  let currentMonthDays = new Date(year, currentMonth, 0).getDate();
  let finalDays = 0;

  const CHRISTMAS_MONTH = 12;
  const CHRISTMAS_DAY = 25;
  const DECEMBER_DAYS = 31;

  for (let i = currentMonth; i <= CHRISTMAS_MONTH; i++) {
    currentDays += daysUntilChristmas(i,year);
  }
  finalDays = currentDays - ((DECEMBER_DAYS - CHRISTMAS_DAY) + (currentMonthDays - (currentMonthDays - currenMonthDaysElapsed)));
  return finalDays;
}

function daysUntilChristmas(currentMonth,year) {
  let currentMonthDays = 0;
  switch (currentMonth) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      currentMonthDays = 31;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      currentMonthDays = 30;
      break;
    default:
      currentMonthDays = isLeapYear(2018,year) ? 29 : 28;
      break;
  }
  return currentMonthDays;
}
console.log(nextChristmas(1, 1, 2018)+" days until christmas");
