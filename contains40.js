// create a function that takes a number to search for in an array of integers
// in the range [40, 10 000]. If the array contains a number with the searched
// number more than 2 time return true, otherwise - false
function presentsIn(searchedNumber){
  let counter = 0;
  let numbers = [];

  for (let i = 40; i <= 10000; i++) {
    numbers.push(i.toString());
  }

  for (var i = 0; i < numbers.length; i++) {
    if(numbers[i].includes(searchedNumber)){
      counter++;
    }
  }

  if(counter >= 2){
    return true;
  }
  
  return false;
}

console.log(presentsIn(40));
