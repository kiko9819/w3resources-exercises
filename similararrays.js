// Write a JavaScript program to check whether two arrays of integers of
// same length are similar or not. The arrays will be similar if one array
// can be obtained from another array by swapping at most one pair of elements.
function findSimilarity(arr1, arr2) {
  let areSimilar = true;
  for (var i = 0; i < arr1.length; i++) {
    if (arr1[i] != arr2[i]) {
      let temp = arr2[i];
      arr2[i] = arr2[i + 1];
      arr2[i + 1] = temp;

      if (arr1[i] == arr2[i]) {
        areSimilar = true;
        break;
      }
    }
  }
  return areSimilar;
}
console.log(findSimilarity([1, 2, 3, 4, 5, 6, 7], [1, 2, 3, 5, 4, 6, 7]));
console.log(findSimilarity([1, 2, 3, 4, 5, 6, 7], [1, 2, 3, 4, 5, 6, 7]));
