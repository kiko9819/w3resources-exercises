// create a function that takes a string and return a new one without the first
// letter.
function popShift(str) {
  return str.substring(1, str.length - 1);
}
// function popShift(str){
//   let strArr = str.split("");
//   strArr.shift();
//   strArr.pop();
//   return strArr.join("");
// }
const popShift2 = str => {
  return str.slice(1, -1);
}

console.log(popShift("hello"));
console.log(popShift2("hello"));
