// create a function that takes a string and returns a new one with the first
// char attached to the front and back
function addChar(str){
  let firstChar = str.split("").shift();

  return firstChar+str+firstChar;
}
console.log(addChar("level"));
