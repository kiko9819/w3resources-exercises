// Write a JavaScript program to find the types of a given angle
function typeOfTriangle(angle) {
  let type = "";

  if (angle > 0 && angle < 90) {
    type = "acute";
  } else if (angle == 90) {
    type = "right";
  } else if (angle > 90 && angle < 180) {
    type = "obtuse";
  } else {
    type = "straight";
  }
  return type;
}
console.log(typeOfTriangle(80));
console.log(typeOfTriangle(90));
console.log(typeOfTriangle(120));
console.log(typeOfTriangle(180));
