// create a function that takes a string and returns 4 copies of that strings
// last three letters
function copy(str){
  if(str.length<3){
    return;
  }
  return str.substring(str.length-3).repeat(3);
}

console.log(copy("abd"));
console.log(copy("abasdd"));
