// create a function that gets the current time in this format
// "hh PM : mm : ss"
function getCurrentTime() {
  let hour = new Date().getHours();
  let minute = new Date().getMinutes();
  let seconds = new Date().getSeconds();
  let day = new Date().getDay();
  let time = hour + " PM : " + minute + " : " + seconds;

  switch (day) {
    case 1:
      day = "Monday"
      break;
    case 2:
      day = "Tuesday"
      break;
    case 3:
      day = "Wednesday"
      break;
    case 4:
      day = "Thursday"
      break;
    case 5:
      day = "Friday"
      break;
    case 6:
      day = "Saturday"
      break;
    default:
      day = "Sunday"
      break;
  }
  return {
    day,
    time
  }
}

let today = getCurrentTime();
console.log("Today is " + today.day);
console.log("The time is " + today.time);
