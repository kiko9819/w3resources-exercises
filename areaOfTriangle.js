// create a function that calculates the area of a triangle by given three sides
function getTriangleArea(sideA, sideB, sideC) {
  let halfPerimeter = (sideA + sideB + sideC) / 2;
  let p = halfPerimeter;

  return Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
}
console.log(getTriangleArea(5, 6, 7));
