// swaps the first and last element of an array.
const swap = arr => {
  if(arr.length<1){
    return "short array";
  }
  let temp = arr[0];
  arr[0] = arr[arr.length-1];
  arr[arr.length-1] = temp;

  return arr;
};

console.log(swap([1,2,3]));
