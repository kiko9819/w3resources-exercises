// create a function that takes three numbers and compares their last digits
// if they are all equal - return false
// otherwise - false.
function checkLastDigit(number1,number2,number3){
  let num1Digit = number1.toString().split("").pop();
  let num2Digit = number2.toString().split("").pop();
  let num3Digit = number3.toString().split("").pop();

  if(num1Digit == num2Digit && num1Digit == num3Digit){
    return true;
  }
  return false;
}
console.log(checkLastDigit(123,23,3));
console.log(checkLastDigit(123,2,3));
