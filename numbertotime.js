// create a function that takes a number like 1245125 and returns a properly
// formatted string that represents that number in hours and minutes.
function timeConverter(time){
  let hours = Math.floor(time/60);
  let minutes = time % 60;
  return `${hours}:${minutes}`;
}

console.log(timeConverter(71));
console.log(timeConverter(450));
console.log(timeConverter(1441));
