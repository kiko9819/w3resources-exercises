// create a function that takes two numbers and return a string saying which
// is what, meaning - if one is positive and the other one is not return the
// number which is positive and that which is not.
function positiveAndNegative(number1,number2){
  let text = "";
  if(number1 > 0 && number2 < 0){
    text = `${number1} is positive and ${number2} is negative.`;
  } else if(number1 < 0 && number2 > 0){
    text = `${number1} is negative and ${number2} is positive`;
  } else {
    text = "Both are either positive or negative";
  }
  return text;
}

console.log(positiveAndNegative(2,-3));
console.log(positiveAndNegative(-2,3));
console.log(positiveAndNegative(2,3));
console.log(positiveAndNegative(-2,-3));
