// create a function that return the current date mm/dd/yyyy
function getCurrentDate(){
  let date = new Date();
  return date.getMonth()+"/"+date.getDay()+"/"+date.getFullYear();
}

console.log(getCurrentDate());
