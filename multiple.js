// create a function that takes a number and tell if that number is a multiple
// of 3 or 7
function isMultiple(number) {
  return number % 3 == 0 || number % 7 == 0 ? "a multiple" : "not a multiple";
}

console.log(`The number 240 is ${isMultiple(240)}`);
