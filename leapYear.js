// create a function that tells if a given year is leap or not.
function leap(year) {
  if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
    return true;
  }
  return false;
}

console.log(leap(1908));
