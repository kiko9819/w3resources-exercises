// create a function that takes an array and returns true of it's first and last
// elements are equal.
function firstLastEqual(arr){
  if(arr.length<2){
    return "Array too short";
  }
  return arr[0]==arr[arr.length-1];
}
console.log(firstLastEqual([1,2,3,1]));
console.log(firstLastEqual([1,1]));
console.log(firstLastEqual([2,1]));
console.log(firstLastEqual([2]));
