// create a function that takes a string and if the word script is on index 4
// return a new string without script.
// otherwise return the whole string
function indexScript(str) {
  let indexOfScript = str.indexOf("script");

  if(indexOfScript == 4){
    return str.split("").slice(0,indexOfScript).join("");
  }
  return str;
}

console.log(indexScript("javascript"));
