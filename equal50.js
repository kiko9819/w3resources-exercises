// create a function that takes two numbers and returns true if either of them
// is equal to 50 or their sum is equal to 50
function equalsOrSum50(number1, number2) {
  let eachIs50 = number1 == 50 || number2 == 50;
  let sumIs50 = (number1+number2) == 50;

  if (eachIs50 || sumIs50){
    return true;
  }
  return false;
}

console.log(equalsOrSum50(50,25));
