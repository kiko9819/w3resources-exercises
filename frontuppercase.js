// create a function that takes a string and returns the string with the
// first three letters made to upper case
function charsToUpper(str) {
  if (str.length <= 3) {
    return str.toUpperCase();
  }
  let front = str.split("")
    .slice(0, 3)
    .join("")
    .toUpperCase();
  return front + str.split("").slice(3).join("");
}

console.log(charsToUpper("hello"));
console.log(charsToUpper("hel"));
console.log(charsToUpper("h"));
console.log(charsToUpper("this is a longer string"));
