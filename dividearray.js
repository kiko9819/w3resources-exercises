// Write a JavaScript code to divide an given array of positive integers
// into two parts. First element goes to first part,
// second element goes to second part, and third element goes to first part
// and so on. Now compute the sum of two parts and store into an array of size two
function divideAndSumHalves(arr) {
  let sumArr = [];
  // check if each of it's elements is a positive number
  let isPositive = arr.every(function(number) {
    return number >= 0;
  });
  if (isPositive) {
    // divide it into two halfs
    let leftHalf = [];
    let rightHalf = [];

    for (let i = 0; i < arr.length / 2; i += 2) {
      leftHalf.push(arr[i]);
    }
    for (let i = arr.length / 2; i < arr.length; i += 2) {
      rightHalf.push(arr[i]);
    }

    leftHalf = leftHalf.reduce(function(prev, next) {
      return prev + next;
    });
    rightHalf = rightHalf.reduce(function(prev, next) {
      return prev + next;
    });

    sumArr.push(leftHalf, rightHalf);
  }
  return sumArr;
}
console.log(divideAndSumHalves([2, 2, 4, 4, 4, 4, 5, 5, 6, 7]));
