// create a function that takes a string and returns true if it contains new or los
// in the beginning.
function beginsWith(str){
  if(str.length<3){
    return;
  }
  return /^((los|new)|(new|los))/gi.test(str);
}

console.log(beginsWith("los angeles"));
console.log(beginsWith("new angeles"));
console.log(beginsWith("asdfangeles"));
