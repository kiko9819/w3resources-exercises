// create a function that takes an array and reverses it.
function reverseIntArr1(arr){
  return arr.reverse();
}
function reverseIntArr2(arr){
  let newArr = [];
  for (let i = arr.length-1; i >=0; i--) {
    newArr.push(arr[i]);
  }
  return newArr;
}
console.log(reverseIntArr([1,2,3]));
console.log(reverseIntArr2([4,5,6]));
