// create a guessing game that plays by itself, generate a random number and
// and compare it to a number you gave to the function.
function game(guess){
  let randomNumber = Math.floor(Math.random()*10+1);
  let playerIsWrong = true;

  if(guess!=randomNumber || guess === "undefined"){
    return false;
  }
  return true;
}
while(!game(2)){
  console.log("Wrong");
}
console.log("You guessed it.");
