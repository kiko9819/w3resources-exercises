// Write a JavaScript to add two positive integers without carry.
function addWithoutCarry(number1, number2) {
  let num1Arr = String(number1).split(""),
    num2Arr = String(number2).split(""),
    diff = Math.abs(num1Arr.length - num2Arr.length),
    result = [];
    
  if (num1Arr.length > num2Arr.length) {
    for (let i = 0; i < diff; i++) {
      num2Arr.unshift('0');
    }
  }
  if (num1Arr.length < num2Arr.length) {
    for (let i = 0; i < diff; i++) {
      num1Arr.unshift('0');
    }
  }
  for (let i = 0; i < num1Arr.length; i++) {
    let currentResult = Number(num1Arr[i]) + Number(num2Arr[i]);
    if (currentResult > 10) {
      result.unshift(currentResult % 10);
    } else {
      result.push(currentResult);
    }
  }
  return result;
}
// console.log(addWithoutCarry(923, 422));
console.log(addWithoutCarry(923, 22));
