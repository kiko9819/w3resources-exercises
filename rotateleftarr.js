// create a function that takes an array and returns it rotated.
function rotateLeft(arr){
  if(arr.length<2){
    return;
  }
  let left = arr.shift();
  arr.push(left);

  return arr;
}

console.log(rotateLeft([1,2,3]));
