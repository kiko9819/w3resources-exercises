// Write a JavaScript program to check whether two given integers are similar
// or not, if a given divisor divides both integers and it does not divide either.
function areSimilar(num1, num2, divisor) {
  let result = (num1 % divisor == 0 && num2 % divisor == 0) ||
    (num1 % divisor != 0 && num2 % divisor != 0);
  return result;
}

console.log(areSimilar(10, 25, 5))
console.log(areSimilar(10, 20, 5))
console.log(areSimilar(10, 20, 4))
