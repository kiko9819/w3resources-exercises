// create a function that takes an array of two numbers and returns true they are
// equal and the their value is 30 or 40, otherwise - false.
// example 40,40 - 30,30
const contains30or40 = arr => {
  if (arr.length < 2) {
    return "Short array";
  }
  if(arr[0]==arr[1]){
    if(arr[0]==30 || arr[0] == 40){
      return true;
    } else {
      return false;
    }
  }
  return false;
};
console.log(contains30or40([30, 30]));
console.log(contains30or40([40, 30]));
console.log(contains30or40([30, 40]));
console.log(contains30or40([40, 40]));
