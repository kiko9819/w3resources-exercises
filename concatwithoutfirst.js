// create a function that takes two strings and concatenates them without
// their first letters
function concatenate(str1, str2) {
  let strArr1 = str1.split("");
  let strArr2 = str2.split("");
  strArr1.shift();
  strArr2.shift();

  return strArr1.join("").concat(strArr2.join(""));
}
const concatenate2 = (str1, str2) => {
  let str1Substring = str1.substring(1);
  let str2Substring = str2.substring(1);

  return str1Substring.concat(str2Substring);
};
const concatenate3 = (str1, str2) => {
  return str1.substring(1)+str2.substring(1);
};
console.log(concatenate("hello", "world"));
console.log(concatenate2("hello", "world"));
console.log(concatenate3("hello", "world"));
