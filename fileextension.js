// create a function that returns the file extension of a file
// it should take a string which represents a filename and it's extension
function getExtension(filename){
  return filename.split(".").pop();
}

console.log(getExtension("thisfile"));
