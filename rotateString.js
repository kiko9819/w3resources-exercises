// create a function that takes a given string and rotates it to the left.
function rotate(str) {
  str = Array.from(str);

  let left = str.shift();
  str.push(left);

  return str;
}
console.log(rotate("w3resource"));
