// create a function that takes a string and a number
// the number will be the number of char to remove from the back and
// front of the string.
function firstAndLastChars(str, n) {
  if (str.length >= n) {
    return str.split("").slice(0, n) +
      str.split("").slice(-n);
  }
  return;
}

const firstLast = (str,n) =>{
  let front = str.substring(0,n);
  let back = str.substring(str.length-n);

  return front.concat(back);
}

console.log(firstAndLastChars("Help", 1));
console.log(firstLast("Help", 1));
