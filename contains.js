// create a function that takes a string and a character
// should return a string with how many times that specified character is met
// in the string, the char must be met in the range [2,4]
function containsChar(char,str){
  let counter = 0;
  let strArr = str.split("");
  for (var i = 0; i < strArr.length; i++) {
    if(strArr[i] == char){
      counter++;
    }
  }
  if(counter >=2 && counter<=4){
    console.log(`Contains ${counter} of the specified char.`);
  } else {
    console.log(`Contains more or less than the range.`);
  }
}

containsChar("l","hello");
