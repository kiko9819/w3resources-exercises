// create a function that takes an array.
// it should tell if it does not contain 1 or 3
const doesNotContain = arr => {
  return (!(arr.length == 2 && (arr.includes(1) || arr.includes(3))))
};
if(doesNotContain([2,2])){
  console.log("does not contain");
} else {
  console.log("contains");
}
