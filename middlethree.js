// create a function that takes a string and returns the three characters
// that are in the middle of the string.
function middleThreeChars(str){
  if(str.length%2==0){
    return;
  }
  str = str.split("");

  return str.splice(str.length/2-1,3);
}
console.log(middleThreeChars("Hello"));
console.log(middleThreeChars("Marshmellowzz"));
console.log(middleThreeChars("Beerpongg"));
console.log(middleThreeChars("Hello"));
