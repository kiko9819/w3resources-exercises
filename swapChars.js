// create a function that swaps the first and last letters of string.
function swapFirstLast(str) {
  if (str === "") {
    return "Empty string";
  }

  if (str.length > 1) {
    let first = str.split("").shift();
    let last = str.split("").pop();
    let middle = str.split("").slice(1, str.length - 1).join("");

    return last + middle + first;
  }
  return str;
}
console.log(swapFirstLast(""));
