// create a function that checks if three numbers end with the same digit.
function sameEndDigit(number1, number2, number3) {
  let num1RightMost = number1.toString().split("").pop();
  let num2RightMost = number2.toString().split("").pop();
  let num3RightMost = number3.toString().split("").pop();

  let counter = 0;
  let rightMostDigits = [num1RightMost, num2RightMost, num3RightMost];

  for (var i = 0; i < rightMostDigits.length; i++) {
    if (rightMostDigits[i] == rightMostDigits[i + 1]) {
      counter++;
    }
  }
  counter+=1;
  if(counter>=2){
    return true;
  }
  return false;
}

console.log(sameEndDigit(23, 23, 23));
console.log(sameEndDigit(23, 23, 2));
console.log(sameEndDigit(23, 24, 2));
