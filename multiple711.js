// create a function that takes two numbers, if one is a multiple of 7 or 11 and
// the other is not return the one that is.
function isMultiple(number1, number2) {
  if (number1 < 0 || number2 < 0) {
    return;
  }

  let isNum1Multiple = number1 % 7 == 0 || number1 % 11 == 0;
  let isNum2Multiple = number2 % 7 == 0 || number2 % 11 == 0;

  if(isNum1Multiple && !isNum2Multiple){
    return number1;
  } else if(!isNum1Multiple && isNum2Multiple){
    return number2;
  } else {
    return "There is no multiples.";
  }
}

console.log(isMultiple(14,23));
