// create a function that takes three numbers and returns true if one of the
// numbers is greater than 20 and the others aren't
function greaterThan20(number1,number2,number3){
  let num1Greater = number1 >=20 && (number1<number2 || number1 < number3);
  let num2Greater = number2 >=20 && (number2<number1 || number2 < number3);
  let num3Greater = number3 >=20 && (number3<number2 || number3 < number1);

  if(num1Greater || num2Greater || num3Greater){
    return true;
  }
  return false;
}

console.log(greaterThan20(23,45,10));
console.log(greaterThan20(23,23,10));
console.log(greaterThan20(21,66,75));
