// create a function that takes two numbers and if one is equal to the other
// return their sum, multiplied by 3, otherwise - just their sum.
function specialSum(operand1, operand2) {
  const MULTIPLICATOR = 3;
  let error = "";
  let operand1Missing = operand1 == undefined || operand1 == null;
  let operand2Missing = operand2 == undefined || operand2 == null;

  if (operand1Missing || operand2Missing) {
    error = "Something is missing.";
    return error;
  }

  if (operand1 === operand2) {
    return MULTIPLICATOR*(operand1 + operand2);
  }
  return operand1+operand2;
}

console.log(specialSum(3,3));
