// create a function that takes an array of strings and returns the largest
// string of them all.
function findLongestWord(str) {
  let longestWord = str.reduce(function(longest, current) {
    return current.length > longest.length ? current : longest;
  });
  return longestWord.length;
}
function getLongestWord(str){
  let max = "";
  for (var i = 0; i < str.length; i++) {
    if(str[i].length > max.length){
      max = str[i];
    }
  }
  return max;
}
console.log(findLongestWord(["15","12","1111","1","11","111"]));
console.log(getLongestWord(["15","12","1111","1","11","111"]));
