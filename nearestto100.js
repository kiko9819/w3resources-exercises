// create a function that takes two numbers and tells which one is closer to 100
function nearestTo100(number1,number2){
  let near1 = Math.abs(100-number1);
  let near2 = Math.abs(100-number2);

  return near1 > near2 ? number2 : number1;
}

console.log(nearestTo100(120,70));
