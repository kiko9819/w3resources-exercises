// create a function that takes a string returns a new one with it's tail(last 3 chars)
// moved to the front.
const moveToFront = str => {
  if(str.length<3){
    return;
  }

  let tail = str.split("").slice(-3).join("");

  return tail+str.split("").slice(0,str.length-3).join("");
};
console.log(moveToFront("Hello"));
console.log(moveToFront("23"));
